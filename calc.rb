#! /usr/bin/env ruby
require 'csv'
require 'bigdecimal'

csv = CSV.new(STDIN)
header = csv.shift
balance = BigDecimal("0")
start_date = nil

RATES = {
  Date.parse('2010-01-01') => BigDecimal("0.065"),
  Date.parse('2015-10-01') => BigDecimal("0.06"),
  Date.parse('2016-01-01') => BigDecimal("0.05"),
  Date.parse('2016-04-01') => BigDecimal("0.045"),

}


class BigDecimal
  def to_dollars
    "$" + round(2).to_s('F')
  end

  def to_pc
    "%" + (self * 100).round(2).to_s('F')
  end

end

def find_rate(date)
  RATES.to_a.reverse.find {|after, rate| date >= after }.last
end

def calc_interest(balance, rate)
  balance * (rate / 365)
end

class Payments
  def initialize(csv)
    @csv = csv
  end

  def peek_date
    peek[:date]
  end

  def next
    value = peek
    @peek = nil
    value
  end

  def has_more?
    !peek.nil?
  end

  def peek
    if @peek.nil?
      @peek = parse(next_row)
    end
    @peek
  end

  def next_row
    while true
      row = @csv.shift
      return nil if row.nil?
      next if row.any? {|str| str.nil? || str.empty? }

      return row
    end
  end

  def parse(row)
    row && {
      date: Date.parse(row[0]),
      amount: BigDecimal(row[1])
    }
  end
end

payments = Payments.new(csv)


starting = csv.shift
start_date = Date.parse(starting[0])
opening_balance = BigDecimal(starting[1])
balance = opening_balance
last_date = start_date

STDERR.puts("opening balance is #{balance.to_dollars} at #{start_date}")

output = CSV.new(STDOUT)

output << ["date", "opening balance", "payment", "rate", "daily interest", "interest_paid", "closing balance"]

monthly_interest = BigDecimal("0")

while payments.has_more?

  opening_balance = balance

  payment =
    if payments.peek_date == last_date
      payments.next[:amount]
    else
      BigDecimal("0")
    end

  balance += payment

  rate = find_rate(last_date)
  interest_amount = calc_interest(balance, rate).abs
  monthly_interest += interest_amount

  next_date = last_date + 1

  interest_paid =
    if next_date.month != last_date.month
      value = monthly_interest
      monthly_interest = BigDecimal("0")
      value
    else
      BigDecimal("0")
    end

  balance -= interest_paid

  output << [last_date, opening_balance.to_dollars, payment.to_dollars, rate.to_pc, interest_amount.to_dollars, interest_paid.to_dollars, balance.to_dollars]

  last_date = next_date

end

STDERR.puts("closing balance is #{balance.to_dollars} at #{last_date}")
